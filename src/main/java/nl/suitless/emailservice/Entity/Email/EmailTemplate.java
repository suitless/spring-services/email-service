package nl.suitless.emailservice.Entity.Email;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity( name = "email_template" )
public class EmailTemplate {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    @NotNull
    private String name;
    @Column(name = "email_type")
    @NotNull
    private EmailType emailType;
    @NotNull
    @Column(name = "template_body", length = 7000)
    private String templateBody;
    @NotNull
    @Column(name = "template_subject")
    private String templateSubject;
    @NotNull
    private Date created;
    @NotNull
    private Date updated;

    public EmailTemplate() {
    }

    public EmailTemplate(String id, @NotNull String name, @NotNull EmailType emailType, @NotNull String templateBody, @NotNull String templateSubject, @NotNull Date created, @NotNull Date updated) {
        this.id = id;
        this.name = name;
        this.emailType = emailType;
        this.templateBody = templateBody;
        this.templateSubject = templateSubject;
        this.created = created;
        this.updated = updated;
    }

    public EmailTemplate(@NotNull String name, @NotNull EmailType emailType, @NotNull String templateBody, @NotNull String templateSubject) {
        this.name = name;
        this.emailType = emailType;
        this.templateBody = templateBody;
        this.templateSubject = templateSubject;
        this.created = new Date();
        this.updated = new Date();
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EmailType getEmailType() {
        return emailType;
    }

    public void setEmailType(EmailType emailType) {
        this.emailType = emailType;
    }

    public String getTemplateBody() {
        return templateBody;
    }

    public void setTemplateBody(String templateBody) {
        this.templateBody = templateBody;
    }

    public String getTemplateSubject() {
        return templateSubject;
    }

    public void setTemplateSubject(String templateSubject) {
        this.templateSubject = templateSubject;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
