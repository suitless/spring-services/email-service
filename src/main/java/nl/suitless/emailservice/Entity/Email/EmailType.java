package nl.suitless.emailservice.Entity.Email;

public enum EmailType {
    TEXT, HTML;

    @Override
    public String toString() {
        switch (this) {
            case HTML:
                return "text/html; charset=utf-8";
            case TEXT:
                return "utf-8";
            default:
                return "utf-8";
        }
    }
}
