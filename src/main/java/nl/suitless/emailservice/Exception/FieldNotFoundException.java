package nl.suitless.emailservice.Exception;

public class FieldNotFoundException extends RuntimeException {
    public FieldNotFoundException(String exception) {
        super(exception);
    }
}
