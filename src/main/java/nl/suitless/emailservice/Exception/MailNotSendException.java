package nl.suitless.emailservice.Exception;

public class MailNotSendException extends RuntimeException {
    public MailNotSendException(String exception) {
        super(exception);
    }
}
