package nl.suitless.emailservice.Exception;

public class TemplateNotFoundException extends RuntimeException {
    public TemplateNotFoundException(String exception) {
        super(exception);
    }
}
