package nl.suitless.emailservice.Exception;

public class AccountNotFoundException extends RuntimeException {
    public AccountNotFoundException(String exception) {
        super(exception);
    }
}
