package nl.suitless.emailservice.Exception;

public class RoleNotFoundException extends RuntimeException {
    public RoleNotFoundException(String exception) {
        super(exception);
    }
}
