package nl.suitless.emailservice.Exception;

public class TemplateAlreadyExistsException extends RuntimeException {
    public TemplateAlreadyExistsException(String exception) {
        super(exception);
    }
}
