package nl.suitless.emailservice.Service.Logic;

import nl.suitless.emailservice.Entity.Account.Account;
import nl.suitless.emailservice.Entity.Email.EmailTemplate;
import nl.suitless.emailservice.Exception.MailNotSendException;
import nl.suitless.emailservice.Service.Interfaces.IAccountService;
import nl.suitless.emailservice.Service.Interfaces.IEmailService;
import nl.suitless.emailservice.Service.Interfaces.ITemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class EmailService implements IEmailService {
    private JavaMailSender emailSender;
    private ITemplateService templateService;
    private IAccountService accountService;
    private String sender;

    @Autowired
    public EmailService(JavaMailSender emailSender, ITemplateService templateService, IAccountService accountService, @Value("${spring.mail.username}") String sender) {
        this.emailSender = emailSender;
        this.accountService = accountService;
        this.templateService = templateService;
        this.sender = String.format("Suitless <%s>", sender);
    }

    @Override
    public void sendTextEmail(String recipient, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(recipient);
        message.setSubject(subject);
        message.setText(text);
        message.setFrom(sender);

        emailSender.send(message);
    }

    @Override
    public void sendTextEmail(String[] recipients, String subject, String text) {
        Arrays.stream(recipients).forEach(x -> sendTextEmail(x, subject, text));
    }

    @Override
    public void sendTextEmailMass(String role, String subject, String text) {
        List<Account> accounts = accountService.getAccountsWithRole(role);
        Arrays.stream(accounts.toArray()).forEach( account -> {
            Account a = (Account) account;
            sendTextEmail(a.getEmail(), subject, text);
        });
    }

    @Override
    public void sendTemplateEmail(String template, String recipient, String... templateArgs) {
        EmailTemplate emailTemplate = templateService.getTemplateForName(template);
        MimeMessagePreparator message = templateService.templateToMail(emailTemplate, recipient, sender, templateArgs);

        try{
            this.emailSender.send(message);
        }catch (MailException e) {
            throw new MailNotSendException("Mail to: " + recipient + " could not be send");
        }
    }

    @Override
    public void sendTemplateEmail(String template, String[] recipients, String... templateArgs) {
        Arrays.stream(recipients).forEach(x -> sendTemplateEmail(template, x, templateArgs));
    }

    @Override
    public void sendTemplateEmailMass(String template, String role, String... templateArgs) {
        List<Account> accounts = accountService.getAccountsWithRole(role);
        Arrays.stream(accounts.toArray()).forEach( account -> {
            Account a = (Account) account;
            sendTemplateEmail(template, a.getEmail(), templateArgs);
        });
    }
}
