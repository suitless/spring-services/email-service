package nl.suitless.emailservice.Service.Logic;

import nl.suitless.emailservice.Data.AccountRepositories.IResetPasswordRepository;
import nl.suitless.emailservice.Entity.Account.Account;
import nl.suitless.emailservice.Entity.Account.ResetPassword;
import nl.suitless.emailservice.Exception.AccountNotFoundException;
import nl.suitless.emailservice.Data.AccountRepositories.IAccountRepository;
import nl.suitless.emailservice.Data.AccountRepositories.IRoleRepository;
import nl.suitless.emailservice.Service.Interfaces.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccountService implements IAccountService {

    private IAccountRepository accountRepository;
    private IRoleRepository roleRepository;
    private IResetPasswordRepository resetPasswordRepository;

    @Autowired
    public AccountService(IAccountRepository accountRepository, IRoleRepository roleRepository, IResetPasswordRepository resetPasswordRepository) {
        this.accountRepository = accountRepository;
        this.roleRepository = roleRepository;
        this.resetPasswordRepository = resetPasswordRepository;
    }

    @Override
    public Account getAccount(String email) {
        Account account = accountRepository.findByEmail(email).orElseThrow(() -> new AccountNotFoundException("Account with email : " + email + " Not found"));
        account.setPassword("");
        return account;
    }

    @Override
    public List<Account> getAccountsWithRole(String role) {
        List<Account> result = accountRepository.findByRoles_name(role);

        return result;
    }

    @Override
    public String getResetPasswordToken(String email) {
        Optional<ResetPassword> foundResetPassword = resetPasswordRepository.findByEmail(email);
        if(foundResetPassword.isPresent()) {
            ResetPassword resetPassword = foundResetPassword.get();
            return  resetPassword.getResetPasswordUrl() + resetPassword.getResetToken();
        } else {
            //You don't want to throw any errors because sometimes the email purpose is not to reset your password.
            //And therefore you won't always get a reset token.
            return "No reset token found";
        }
    }


}
