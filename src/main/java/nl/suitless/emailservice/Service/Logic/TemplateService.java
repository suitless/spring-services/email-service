package nl.suitless.emailservice.Service.Logic;

import nl.suitless.emailservice.Data.EmailRepositories.IEmailTemplateRepository;
import nl.suitless.emailservice.Entity.Account.Account;
import nl.suitless.emailservice.Entity.Email.EmailTemplate;
import nl.suitless.emailservice.Exception.FieldNotFoundException;
import nl.suitless.emailservice.Exception.TemplateAlreadyExistsException;
import nl.suitless.emailservice.Exception.TemplateNotFoundException;
import nl.suitless.emailservice.Service.Interfaces.IAccountService;
import nl.suitless.emailservice.Service.Interfaces.ITemplateService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Optional;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class TemplateService implements ITemplateService {
    private IEmailTemplateRepository emailTemplateRepository;
    private IAccountService accountService;
    private Pattern modulePattern;
    private String propertyRegex = "\\{\\{(?<object>\\w*).?(?<field>\\w*)}}";


    @Autowired
    public TemplateService(IEmailTemplateRepository emailTemplateRepository, IAccountService accountService) {
        this.emailTemplateRepository = emailTemplateRepository;
        this.accountService = accountService;
        this.modulePattern = Pattern.compile(propertyRegex);
    }

    @Override
    public EmailTemplate getTemplateForID(String ID) {
        return getTemplateIfPresent(ID);
    }

    @Override
    public EmailTemplate getTemplateForName(String name) {
        return emailTemplateRepository.findByName(name).orElseThrow(() -> new TemplateNotFoundException("Template with name: " + name + " not found"));
    }

    @Override
    public void createTemplate(EmailTemplate template) {
        checkIfTemplateAlreadyExists(template.getName());
        emailTemplateRepository.save(template);
    }


    @Override
    public void updateTemplate(EmailTemplate template) {
        EmailTemplate oldTemplate = getTemplateForName(template.getName());
        oldTemplate.setUpdated(new Date());
        oldTemplate.setTemplateBody(template.getTemplateBody());
        oldTemplate.setTemplateSubject(template.getTemplateSubject());
        emailTemplateRepository.save(oldTemplate);
    }

    @Override
    public void deleteTemplate(EmailTemplate template) {
        EmailTemplate foundTemplate = getTemplateIfPresent(template.getId());
        emailTemplateRepository.delete(foundTemplate);
    }

    public ArrayList<EmailTemplate> getAllTemplates() {
        return Lists.newArrayList(emailTemplateRepository.findAll().iterator());
    }

    @Override
    public MimeMessagePreparator templateToMail(EmailTemplate template, String recipient, String sender, String... templateArgs) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Account account = accountService.getAccount(recipient);
        String resetToken = accountService.getResetPasswordToken(account.getEmail());

        template.setTemplateBody(propertyBindString(template.getTemplateBody(), account, resetToken, sender, df.format(new Date())));

        if(templateArgs.length > 0) {
            template.setTemplateBody(String.format(template.getTemplateBody(), templateArgs));
        }

        return preparator -> {
            preparator.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
            preparator.setFrom(sender);
            preparator.setContent(template.getTemplateBody(), template.getEmailType().toString());
            preparator.setSubject(template.getTemplateSubject());
        };
    }

    private String propertyBindString(String input, Account account, String resetToken, String sender, String date){
        HashMap<String, Object> binder = new HashMap<>();
        binder.put("account", account);
        binder.put("sender", sender);
        binder.put("date", date);
        binder.put("resetToken", resetToken);

        Matcher m = modulePattern.matcher(input);
        return m.replaceAll((MatchResult result) -> {
            Object replace = binder.get(m.group(1));
            if(!m.group(2).equals("")){ //Object binding.
                for (Field field : replace.getClass().getDeclaredFields()) {
                    field.setAccessible(true);
                    if(field.getName().equals(m.group(2))){ //Requested field
                        try {
                            return (String) field.get(replace);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                }
                throw new FieldNotFoundException(String.format("Field: %s.%s was not found.", m.group(0), m.group(1)));
            }  else { //String replacement
                return (String) replace;
            }
        });
    }

    //region Generic methods
    private EmailTemplate getTemplateIfPresent(String id){
        return emailTemplateRepository.findById(id).orElseThrow(() -> new TemplateNotFoundException("Template with id: " + id + " not found"));
    }

    private void checkIfTemplateAlreadyExists(String name){
        Optional<EmailTemplate> foundTemplate = emailTemplateRepository.findByName(name);
        if(foundTemplate.isPresent()){
            throw new TemplateAlreadyExistsException("Template with name: " + name + " already exists");
        }
    }
    //endregion

}
