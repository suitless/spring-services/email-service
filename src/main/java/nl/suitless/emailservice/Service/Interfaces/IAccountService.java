package nl.suitless.emailservice.Service.Interfaces;

import nl.suitless.emailservice.Entity.Account.Account;

import java.util.List;

public interface IAccountService {
    /**
     * Get the account by email
     * @param email the email the user registered with
     * @return <b>Account</b> of the user
     */
    Account getAccount(String email);

    /**
     * get all the accounts that contain a specific role
     * @param role the role the accounts need to have
     * @return a list of <b>Account</b> entities
     */
    List<Account> getAccountsWithRole(String role);

    /**
     * get the reset password token from a specific account
     * if no token was found it will just return a string because you dont want to throw any errors
     * @param email the email of the user's account
     * @return A reset password token
     */
    String getResetPasswordToken(String email);
}
