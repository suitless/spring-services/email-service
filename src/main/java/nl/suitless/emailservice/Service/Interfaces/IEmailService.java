package nl.suitless.emailservice.Service.Interfaces;

import org.springframework.mail.SimpleMailMessage;

public interface IEmailService {
    void sendTextEmail(String recipient, String subject, String text);
    void sendTextEmail(String[] recipients, String subject, String text);
    void sendTextEmailMass(String role, String subject, String text);

    void sendTemplateEmail(String template, String recipient, String... templateArgs);
    void sendTemplateEmail(String template, String[] recipients, String... templateArgs);
    void sendTemplateEmailMass(String template, String role, String... templateArgs);
}
