package nl.suitless.emailservice.Service.Interfaces;

import nl.suitless.emailservice.Entity.Email.EmailTemplate;
import org.springframework.mail.javamail.MimeMessagePreparator;

import java.util.ArrayList;

public interface ITemplateService {
    EmailTemplate getTemplateForID(String ID);
    EmailTemplate getTemplateForName(String name);
    void createTemplate(EmailTemplate template);
    void updateTemplate(EmailTemplate template);
    void deleteTemplate(EmailTemplate template);
    ArrayList<EmailTemplate> getAllTemplates();
    MimeMessagePreparator templateToMail(EmailTemplate template, String recipient, String sender, String... templateArgs);
}
