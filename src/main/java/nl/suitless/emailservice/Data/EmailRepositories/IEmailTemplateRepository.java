package nl.suitless.emailservice.Data.EmailRepositories;

import nl.suitless.emailservice.Entity.Email.EmailTemplate;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface IEmailTemplateRepository extends CrudRepository<EmailTemplate, Integer> {
    Optional<EmailTemplate> findByName(String name);
    Optional<EmailTemplate> findById(String id);
}
