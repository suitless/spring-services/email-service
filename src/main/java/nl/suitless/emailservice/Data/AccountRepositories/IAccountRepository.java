package nl.suitless.emailservice.Data.AccountRepositories;

import nl.suitless.emailservice.Entity.Account.Account;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface IAccountRepository extends CrudRepository<Account, Integer> {
    Optional<Account> findByEmail(String email);
    List<Account> findByRoles_name(String name);
}
