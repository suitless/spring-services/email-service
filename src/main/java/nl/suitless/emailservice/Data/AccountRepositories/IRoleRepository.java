package nl.suitless.emailservice.Data.AccountRepositories;

import nl.suitless.emailservice.Entity.Account.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface IRoleRepository extends CrudRepository<Role, Integer> {
}
