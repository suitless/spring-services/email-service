package nl.suitless.emailservice.Data.AccountRepositories;

import nl.suitless.emailservice.Entity.Account.ResetPassword;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface IResetPasswordRepository extends CrudRepository<ResetPassword, Integer> {
    Optional<ResetPassword> findByEmail(String email);
}
