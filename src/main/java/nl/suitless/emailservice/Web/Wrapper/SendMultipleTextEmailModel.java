package nl.suitless.emailservice.Web.Wrapper;

import javax.validation.constraints.NotNull;

public class SendMultipleTextEmailModel {
    @NotNull
    private String[] emails;
    @NotNull
    private String subject;
    @NotNull
    private String body;

    public String[] getEmails() {
        return emails;
    }

    public void setEmails(String[] emails) {
        this.emails = emails;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
