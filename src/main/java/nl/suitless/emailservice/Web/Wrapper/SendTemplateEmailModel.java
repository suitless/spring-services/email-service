package nl.suitless.emailservice.Web.Wrapper;

import javax.validation.constraints.NotNull;

public class SendTemplateEmailModel {
    @NotNull
    private String email;
    @NotNull
    private String templateName;
    @NotNull
    private String[] bodyArgs;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String[] getBodyArgs() {
        return bodyArgs;
    }

    public void setBodyArgs(String[] bodyArgs) {
        this.bodyArgs = bodyArgs;
    }
}
