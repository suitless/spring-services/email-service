package nl.suitless.emailservice.Web.Wrapper;

import nl.suitless.emailservice.Entity.Email.EmailType;

import javax.validation.constraints.NotNull;

public class EmailTemplateWrapper {
    @NotNull
    private String name;
    @NotNull
    private EmailType emailType;
    @NotNull
    private String templateBody;
    @NotNull
    private String templateSubject;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EmailType getEmailType() {
        return emailType;
    }

    public void setEmailType(EmailType emailType) {
        this.emailType = emailType;
    }

    public String getTemplateBody() {
        return templateBody;
    }

    public void setTemplateBody(String templateBody) {
        this.templateBody = templateBody;
    }

    public String getTemplateSubject() {
        return templateSubject;
    }

    public void setTemplateSubject(String templateSubject) {
        this.templateSubject = templateSubject;
    }
}
