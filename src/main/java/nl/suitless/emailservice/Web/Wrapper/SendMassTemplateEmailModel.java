package nl.suitless.emailservice.Web.Wrapper;

import javax.validation.constraints.NotNull;

public class SendMassTemplateEmailModel {
    @NotNull
    private String role;
    @NotNull
    private String templateName;
    @NotNull
    private String bodyArgs;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getBodyArgs() {
        return bodyArgs;
    }

    public void setBodyArgs(String bodyArgs) {
        this.bodyArgs = bodyArgs;
    }
}
