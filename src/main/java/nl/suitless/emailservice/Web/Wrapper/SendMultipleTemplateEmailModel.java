package nl.suitless.emailservice.Web.Wrapper;

import javax.validation.constraints.NotNull;

public class SendMultipleTemplateEmailModel {
    @NotNull
    private String[] emails;
    @NotNull
    private String templateName;
    @NotNull
    private String[] bodyArgs;

    public String[] getEmails() {
        return emails;
    }

    public void setEmails(String[] emails) {
        this.emails = emails;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String[] getBodyArgs() {
        return bodyArgs;
    }

    public void setBodyArgs(String[] bodyArgs) {
        this.bodyArgs = bodyArgs;
    }
}
