package nl.suitless.emailservice.Web.Wrapper;

import javax.validation.constraints.NotNull;

public class SendTextEmailModel {
    @NotNull
    private String email;
    @NotNull
    private String subject;
    @NotNull
    private String body;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
