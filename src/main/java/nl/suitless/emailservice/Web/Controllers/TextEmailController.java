package nl.suitless.emailservice.Web.Controllers;


import nl.suitless.emailservice.Service.Interfaces.IEmailService;
import nl.suitless.emailservice.Web.Wrapper.SendMassTextEmailModel;
import nl.suitless.emailservice.Web.Wrapper.SendMultipleTextEmailModel;
import nl.suitless.emailservice.Web.Wrapper.SendTextEmailModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/send/text")
public class TextEmailController {

    private IEmailService emailService;

    @Autowired
    public TextEmailController(IEmailService emailService) {
        this.emailService = emailService;
    }

    @PostMapping("/single")
    public ResponseEntity<SendTextEmailModel> sendEmail(@RequestBody SendTextEmailModel request){
        emailService.sendTextEmail(request.getEmail(), request.getSubject(), request.getBody());
        return new ResponseEntity<>(request, HttpStatus.OK);
    }

    @PostMapping("/multi")
    public ResponseEntity<SendMultipleTextEmailModel> sendMultipleEmail(@RequestBody SendMultipleTextEmailModel request){
        emailService.sendTextEmail(request.getEmails(), request.getSubject(), request.getBody());
        return new ResponseEntity<>(request, HttpStatus.OK);
    }

    @PostMapping("/mass")
    public ResponseEntity<SendMassTextEmailModel> sendMassEmail(@RequestBody SendMassTextEmailModel request){
        emailService.sendTextEmailMass(request.getRole(), request.getSubject(), request.getBody());
        return new ResponseEntity<>(request, HttpStatus.OK);
    }
}
