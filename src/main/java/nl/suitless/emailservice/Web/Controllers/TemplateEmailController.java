package nl.suitless.emailservice.Web.Controllers;

import nl.suitless.emailservice.Service.Interfaces.IEmailService;
import nl.suitless.emailservice.Web.Wrapper.SendMassTemplateEmailModel;
import nl.suitless.emailservice.Web.Wrapper.SendMultipleTemplateEmailModel;
import nl.suitless.emailservice.Web.Wrapper.SendTemplateEmailModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/send/template")
public class TemplateEmailController {

    private IEmailService emailService;

    @Autowired
    public TemplateEmailController(IEmailService emailService) {
        this.emailService = emailService;
    }

    @PostMapping("/single")
    public ResponseEntity<SendTemplateEmailModel> sendEmail(@RequestBody SendTemplateEmailModel request){
        emailService.sendTemplateEmail(request.getTemplateName(), request.getEmail(), request.getBodyArgs());
        return new ResponseEntity<>(request, HttpStatus.OK);
    }

    @PostMapping("/multi")
    public ResponseEntity<SendMultipleTemplateEmailModel> sendMultipleEmail(@RequestBody SendMultipleTemplateEmailModel request){
        emailService.sendTemplateEmail(request.getTemplateName(), request.getEmails(), request.getBodyArgs());
        return new ResponseEntity<>(request, HttpStatus.OK);
    }

    @PostMapping("/mass")
    public ResponseEntity<SendMassTemplateEmailModel> sendMassEmail(@RequestBody SendMassTemplateEmailModel request){
        emailService.sendTemplateEmailMass(request.getTemplateName(), request.getRole(), request.getBodyArgs());
        return new ResponseEntity<>(request, HttpStatus.OK);
    }
}
