package nl.suitless.emailservice.Web.Controllers;

import nl.suitless.emailservice.Entity.Email.EmailTemplate;
import nl.suitless.emailservice.Service.Interfaces.ITemplateService;
import nl.suitless.emailservice.Web.Wrapper.EmailTemplateWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/template")
public class TemplateController {

    private ITemplateService templateService;

    @Autowired
    public TemplateController(ITemplateService templateService){
        this.templateService = templateService;
    }

    @GetMapping("")
    public ResponseEntity<List<EmailTemplate>> getTemplates() {
        return new ResponseEntity<>(templateService.getAllTemplates(), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<EmailTemplate> postTemplate(@RequestBody EmailTemplateWrapper wrapper){
        EmailTemplate template = new EmailTemplate(wrapper.getName(), wrapper.getEmailType(), wrapper.getTemplateBody(), wrapper.getTemplateSubject());
        templateService.createTemplate(template);
        return new ResponseEntity<>(template, HttpStatus.OK);
    }

    @PutMapping("")
    public ResponseEntity<EmailTemplate> putTemplate(@RequestBody EmailTemplateWrapper wrapper){
        EmailTemplate template = new EmailTemplate(wrapper.getName(), wrapper.getEmailType(), wrapper.getTemplateBody(), wrapper.getTemplateSubject());
        templateService.updateTemplate(template);
        return new ResponseEntity<>(template, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTemplate(@PathVariable String id){
        EmailTemplate template = new EmailTemplate();
        template.setId(id);
        templateService.deleteTemplate(template);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
