package nl.suitless.emailservice.Web.Utils;

import nl.suitless.emailservice.Exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

public class RestErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = AccountNotFoundException.class)
    public final ResponseEntity<ErrorDetails> handleAccountNotFoundException (AccountNotFoundException e, WebRequest request){
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = FieldNotFoundException.class)
    public final ResponseEntity<ErrorDetails> handleFieldNotFoundException (FieldNotFoundException e, WebRequest request){
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = MailNotSendException.class)
    public final ResponseEntity<ErrorDetails> handleMailNotSendException (MailNotSendException e, WebRequest request){
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = RoleNotFoundException.class)
    public final ResponseEntity<ErrorDetails> handleRoleNotFoundException (RoleNotFoundException e, WebRequest request){
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = TemplateAlreadyExistsException.class)
    public final ResponseEntity<ErrorDetails> handleTemplateAlreadyExistsException (TemplateAlreadyExistsException e, WebRequest request){
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = TemplateNotFoundException.class)
    public final ResponseEntity<ErrorDetails> handleTemplateNotFoundException (TemplateNotFoundException e, WebRequest request){
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }
}
