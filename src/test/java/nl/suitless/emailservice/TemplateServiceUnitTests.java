package nl.suitless.emailservice;

import nl.suitless.emailservice.Entity.Account.Account;
import nl.suitless.emailservice.Entity.Email.EmailTemplate;
import nl.suitless.emailservice.Entity.Email.EmailType;
import nl.suitless.emailservice.Exception.TemplateAlreadyExistsException;
import nl.suitless.emailservice.Exception.TemplateNotFoundException;
import nl.suitless.emailservice.Data.EmailRepositories.IEmailTemplateRepository;
import nl.suitless.emailservice.Service.Logic.AccountService;
import nl.suitless.emailservice.Service.Logic.TemplateService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mail.javamail.MimeMessagePreparator;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TemplateServiceUnitTests {
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Mock
    private IEmailTemplateRepository emailTemplateRepository;
    @Mock
    private AccountService accountService;
    @InjectMocks
    private TemplateService templateService;

    private EmailTemplate textTemplate;
    private EmailTemplate htmlTemplate;
    private Account mockAccount;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        textTemplate = new EmailTemplate("testTemplate", EmailType.TEXT, "Hello {{account.email}},\ntoday is {{date}}. If you receive this email (%s), testing went wrong. Please report this to it@eindhovenlinc.com. \ngreetings, Suitless", "testTextTemplate");
        htmlTemplate = new EmailTemplate("testTemplate", EmailType.HTML, "<P>Hello {{account.email}},</p><p>today is <b>{{date}}</b>.</p> <p>If you receive this email (%s), testing went wrong. Please report this to <a href=\"mailto:it@eindhovenlinc.com\">it@eindhovenlinc.com</a></p>. <p>greetings, Suitless</p>", "testHtmlTemplate");

        textTemplate.setId("testID1");
        htmlTemplate.setId("testID2");

        mockAccount = new Account("it@eindhovenlinc.com", "password", "Erik", "Johanssen");
    }

    @Test
    public void createTemplateExists() {
        when(emailTemplateRepository.findByName(anyString())).thenReturn(Optional.of(textTemplate));
        exception.expect(TemplateAlreadyExistsException.class);
        templateService.createTemplate(textTemplate);
    }

    @Test
    public void createTemplateValid() {
        when(emailTemplateRepository.findByName(anyString())).thenReturn(Optional.empty());
        templateService.createTemplate(textTemplate);
    }

    @Test
    public void updateTemplate() {
        when(emailTemplateRepository.findByName(anyString())).thenReturn(Optional.of(textTemplate));
        EmailTemplate intermediate = templateService.getTemplateForName(textTemplate.getName());
        intermediate.setTemplateSubject("updated!");
        intermediate.setTemplateBody("updated!");
        templateService.updateTemplate(intermediate);

        EmailTemplate check = templateService.getTemplateForName(textTemplate.getName());
        Assert.assertEquals(check.getTemplateBody(), intermediate.getTemplateBody());
        Assert.assertEquals(check.getTemplateSubject(), intermediate.getTemplateSubject());
    }

    @Test
    public void deleteTemplateValid() {
        when(emailTemplateRepository.findById(anyString())).thenReturn(Optional.of(textTemplate));
        templateService.deleteTemplate(textTemplate);
    }

    @Test
    public void deleteTemplateNotExists() {
        when(emailTemplateRepository.findById(anyString())).thenReturn(Optional.empty());
        exception.expect(TemplateNotFoundException.class);
        templateService.deleteTemplate(textTemplate);
    }

    @Test
    public void templateToMail() throws Exception {
        when(accountService.getAccount(anyString())).thenReturn(mockAccount);

        MimeMessagePreparator preparator = templateService.templateToMail(textTemplate, "it@eindhovenlinc.com", "develop@suitless.nl", "WALDO");
        testPreparator(preparator);
        preparator = templateService.templateToMail(htmlTemplate, "it@eindhovenlinc.com", "develop@suitless.nl", "WALDO");
        testPreparator(preparator);
    }

    private void testPreparator(MimeMessagePreparator preparator) throws IllegalAccessException {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        int fieldOffset = 0;
        for (Field field : preparator.getClass().getDeclaredFields()) {
            field.setAccessible(true); // You might want to set modifier to public first.
            Object value = field.get(preparator);
            switch (fieldOffset++) {
                case 0:
                    Assert.assertEquals("it@eindhovenlinc.com", value);
                    break;
                case 1:
                    Assert.assertEquals("develop@suitless.nl", value);
                    break;
                case 2:
                    EmailTemplate t = (EmailTemplate) value;
                    Assert.assertTrue(t.getTemplateBody().contains("it@eindhovenlinc.com"));
                    Assert.assertTrue(t.getTemplateBody().contains(df.format(new Date())));
                    Assert.assertTrue(t.getTemplateBody().contains("WALDO"));
                    break;
            }
        }
    }
}
