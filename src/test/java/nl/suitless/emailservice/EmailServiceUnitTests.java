package nl.suitless.emailservice;

import nl.suitless.emailservice.Entity.Account.Account;
import nl.suitless.emailservice.Entity.Email.EmailTemplate;
import nl.suitless.emailservice.Entity.Email.EmailType;
import nl.suitless.emailservice.Data.EmailRepositories.IEmailTemplateRepository;
import nl.suitless.emailservice.Service.Logic.AccountService;
import nl.suitless.emailservice.Service.Logic.EmailService;
import nl.suitless.emailservice.Service.Logic.TemplateService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.MessagingException;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EmailServiceUnitTests {
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Mock
    private IEmailTemplateRepository emailTemplateRepository;
    @Mock
    private TemplateService templateService;
    @InjectMocks
    private EmailService emailService;

    @Mock
    private AccountService accountService;

    private EmailTemplate textTemplate;
    private Account mockAccount;
    private List<Account> mockAccountList;

    @Mock
    private JavaMailSender javaMailSender;

    @Bean
    @Primary
    public JavaMailSender javaMailSender() {
        JavaMailSender javaMailSender = this.javaMailSender;
        return javaMailSender;
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        textTemplate = new EmailTemplate("testTemplate", EmailType.TEXT, "Hello {{account.email}},\ntoday is {{date}}. If you receive this email (%s), testing went wrong. Please report this to it@eindhovenlinc.com. \ngreetings, Suitless", "testTextTemplate");

        textTemplate.setId("testID1");

        mockAccount = new Account("it@eindhovenlinc.com", "password", "Erik", "Johanssen");
        mockAccountList = new ArrayList<>();
        mockAccountList.add(mockAccount);
        mockAccountList.add(mockAccount);
        mockAccountList.add(mockAccount);
    }

    @Test
    public void sendTextEmail() throws MessagingException {
        emailService.sendTextEmail("it@eindhovenlinc.com", "testEmail", "why are you seeing this");
    }

    @Test
    public void sendTextEmailList() {
        String[] recipients = {"it@eindhovenlinc.com", "it@eindhovenlinc.com", "it@eindhovenlinc.com"};
        emailService.sendTextEmail(recipients, "testEmail", "why are you seeing this");
    }

    @Test
    public void sendTextEmailMass() {
        when(accountService.getAccountsWithRole(anyString())).thenReturn(mockAccountList);
        emailService.sendTextEmailMass("anystring", "testing", "this is text");

    }

    @Test
    public void sendTemplateEmail() {
        emailService.sendTemplateEmail("testTemplate", "it@eindhovenlinc", "WALDO");
    }

    @Test
    public void sendTemplateEmailList() {
        String[] recipients = {"it@eindhovenlinc.com", "it@eindhovenlinc.com", "it@eindhovenlinc.com"};
        emailService.sendTemplateEmail("testTemplate", recipients, "WALDo");
    }

    @Test
    public void sendtemplateEmailMass() {
        when(accountService.getAccountsWithRole(anyString())).thenReturn(mockAccountList);
        emailService.sendTemplateEmailMass("testTemplate", "anystring", "WALDO");

    }


}
