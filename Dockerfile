FROM openjdk:11.0.4-jre
COPY /build/libs/com.ehvlinc.email-service-0.0.1-SNAPSHOT.jar run.jar
EXPOSE 8888
CMD "java" "-jar" "run.jar"
